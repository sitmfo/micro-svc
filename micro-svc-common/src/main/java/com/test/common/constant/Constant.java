package com.test.common.constant;

/**
 * 系统常量
 * @author yuxue
 * @date 2018-09-07
 */
public class Constant {

	public static final String UTF8 = "UTF-8";

	public static final String JSESSIONID = "JSESSIONID";
	
	public static final String TOKEN = "token";

	public static final String SSO_USER = "sso_user";
    
	public static final String REDIRECT_URL = "redirect_url";
	
	public static String WECHAT_REQ_URL = "https://api.weixin.qq.com/sns/jscode2session";
	

}
